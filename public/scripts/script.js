// ObtÃ©n el select y el textarea
const select = document.getElementById("fonts");
const textarea = document.getElementById("demo-font");

// Agrega un 'event listener' al select que se activarÃ¡ cuando se cambie el valor
select.addEventListener("change", function(){
  // Obtiene la opciÃ³n seleccionada
  const seleccion = select.value;

  // Actualiza el class del textarea para aplicar la tipografÃ­a seleccionada
  textarea.setAttribute("class", seleccion);
});